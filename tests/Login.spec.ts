import {expect, test} from '@playwright/test';

test('has title', async ({page}) => {
    await page.goto('https://dev.anaonline.id/');

    // Expect a title "to contain" a substring.
    await expect(page).toHaveTitle(/Ana Online Landing Page/);
});

test('User Login', async ({page}) => {
    // await page.goto('https://dev.anaonline.id/login');
    await page.goto('https://dev.anaonline.id/anaonline');
    await expect(page).toHaveTitle(/Login/);

    // await page.getByLabel('NPA | Contoh: 04.0001').fill("99.0002")
    // page.getByPlaceholder('NPA | Contoh: 04.0001' ).fill("99.0002");
    await page.type("#npa", "99.0002", {delay: 100});
    await page.type("#password", "12345", {delay: 100});
    // await page.getByLabel('Kata Sandi / Password').fill("12345")
    // await page.getByText('Login').click();
    // page.getByRole('textbox').fill("99.0002");

    // Expects page to have a heading with the name of Installation.
    // await expect(page.getByRole('heading', {name: 'Installation'})).toBeVisible();
    await page.screenshot({path: 'screenshot.png', fullPage: true});
});
