import { expect, test } from '@playwright/test';
import {
  login,
  setPcLength,
  getPcEntriesLength,
  setSearchPc,
  getRowIndex,
  getPWData,
  getPDData,
  getPCData,
  clickNextBtn,
  clickPreviousBtn,
  clickPageBtn
} from '../../actions';

test.beforeEach(async ({ page }) => {
  await login(page, { npa: '99.0002', password: '12345' });
});

test.describe('Testing Dashboard', () => {
  test('Should display PC Table selected length', async ({ page }) => {
    const length = await getPcEntriesLength(page);
    expect(length).toBe(10);

    // Jika user pilih 25
    await setPcLength(page, '25');
    const length2 = await getPcEntriesLength(page);
    expect(length2).toBe(25);

    // Jika user pilih 50
    await setPcLength(page, '50');
    const length3 = await getPcEntriesLength(page);
    expect(length3).toBe(50);

    // Jika user pilih 100
    await setPcLength(page, '100');
    const length1 = await getPcEntriesLength(page);
    expect(length1).toBe(100);

    // Jika user memilih kembali 10
    await setPcLength(page, '10')
    const length4 = await getPcEntriesLength(page);
    expect(length4).toBe(10);
  });

  test('Should display search PC Table', async ({ page }) => {
    // Mencari Data Berdasarkan PW
    await setSearchPc(page, 'Jawa Barat');
    const firstPW = await getPWData(page, 0);
    expect(firstPW).toContain('Jawa Barat');

    // Mencari Data Berdasarkan PD
    await setSearchPc(page, 'Jakarta Utara');
    const firstPD = await getPDData(page, 0);
    expect(firstPD).toContain('Jakarta Utara');

    // Mencari Data Berdasarkan PC
    await setSearchPc(page, 'buahbatu');
    const firstPC = await getPCData(page, 0);
    expect(firstPC).toContain('Buahbatu');
  });

});

test.describe('Testing Pagination PC Table', () => {
  test('Pagination behaviour', async ({ page }) => {
    // Default behaviour
    // Tombol Previous Disabled di page pertama
    const btnClass = await page.locator('#data_pc_previous').getAttribute('class');
    expect(btnClass).toContain('disabled');

    // Click Next Button
    await clickNextBtn(page);
    const btnClass1 = await page.locator('#data_pc_previous').getAttribute('class');
    expect(btnClass1).not.toContain('disabled');
    const btnClass2 = await page.locator('#data_pc_next').getAttribute('class');
    expect(btnClass2).not.toContain('disabled');
    const firstIndexNumber = await getRowIndex(page, 0);
    expect(firstIndexNumber).toBe('11');

    // Click Previous Button
    await clickPreviousBtn(page);
    const firstIndexNumber2 = await getRowIndex(page, 0);
    expect(firstIndexNumber2).toBe('1');

    // Go To Last Page
    // Get List of element class
    await clickPageBtn(page, 6);
    const btnClass3 = await page.locator('#data_pc_next').getAttribute('class');
    expect(btnClass3).toContain('disabled');
  });
});
