import {expect, type Page, test} from '@playwright/test';

test.beforeEach(async ({page}) => {
    await page.goto('https://dev.anaonline.id/login');
});

test.describe('Testing Login', () => {
    test('Should be successfully to input NPA and Password Invalid', async ({page}) => {
        // Masukkan informasi login yang salah
        await page.fill('#npa', 'username_tidak_valid');
        await page.fill('#password', 'password_tidak_valid');

        // Klik tombol login
        await page.click('//button[@type="submit"]');

        // Tunggu sebentar untuk memastikan pesan kesalahan muncul
        await page.waitForTimeout(2000);
        await page.waitForSelector('//p[@class="mb-0"]', { state: 'visible' });

        await page.waitForTimeout(2000);
        // Dapatkan teks dari elemen dan verifikasi
        const errorMessage = await page.textContent('//p[@class="mb-0"]');
        expect(errorMessage).toBe('NPA tidak dikenali sistem');
    });

    test('Should be successfully to input NPA and Password Valid', async ({page}) => {
        // Masukkan informasi login yang salah
        await page.fill('#npa', '99.0002');
        await page.fill('#password', '12345');

        // Klik tombol login
        await page.click('//button[@type="submit"]');

        await page.waitForTimeout(2000);
        // Dapatkan teks dari elemen dan verifikasi
        const verifyTitle = await page.textContent('//h1');
        expect(verifyTitle).toBe('Dashboard');
    });
});