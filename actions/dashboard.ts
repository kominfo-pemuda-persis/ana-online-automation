import { type Page } from '@playwright/test';

export async function gotoDashboard(page: Page) {
  await page.goto('https://dev.anaonline.id/dashboard', { waitUntil: 'networkidle' });
}

export async function setPcLength(page: Page, length: string) {
  const available_options = ['10', '25', '50', '100']
  if (!available_options.includes(length)) {
    throw new Error(`Length must be one of ${available_options.join(', ')}`)
  }

  await page.waitForSelector('//input[@type="search"]', { state: 'visible' });
  await page.selectOption('//select[@name="data_pc_length"]', length)
  await page.waitForTimeout(2000);
}

export async function getPcEntriesLength(page: Page) {
  await page.waitForSelector('//table[@id="data_pc"]//tbody//tr[1]', { state: 'visible' });
  const items = await page.$$('//table[@id="data_pc"]//tbody//tr')
  return items.length
}

export async function setSearchPc(page: Page, search: string) {
  await page.waitForSelector('//input[@type="search"]', { state: 'visible' });
  await page.fill('//input[@type="search"]', search)
}

export async function getRowIndex(page: Page, position: number) {
  await page.waitForSelector('//table[@id="data_pc"]', { state: 'visible' });
  return await page.textContent(`xpath=.//table[@id="data_pc"]//tbody//tr[${position + 1}]//td[1]`)
}

export async function getPWData(page: Page, position: number) {
  await page.waitForSelector('//table[@id="data_pc"]', { state: 'visible' });
  return await page.textContent(`xpath=.//table[@id="data_pc"]//tbody//tr[${position + 1}]//td[2]`)
}

export async function getPDData(page: Page, position: number) {
  await page.waitForSelector('//table[@id="data_pc"]', { state: 'visible' });
  return await page.textContent(`xpath=.//table[@id="data_pc"]//tbody//tr[${position + 1}]//td[3]`)
}

export async function getPCData(page: Page, position: number) {
  await page.waitForSelector('//table[@id="data_pc"]', { state: 'visible' });
  return await page.textContent(`xpath=.//table[@id="data_pc"]//tbody//tr[${position + 1}]//td[4]`)
}

export async function clickNextBtn(page: Page) {
  await page.click('#data_pc_next')
}

export async function clickPreviousBtn(page: Page) {
  await page.click('#data_pc_previous')
}

export async function clickPageBtn(page: Page, position: number) {
  await page.click(`xpath=//a[@data-dt-idx="${position + 1}"]`)
}
