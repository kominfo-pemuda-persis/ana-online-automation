import { type Page } from '@playwright/test';

export async function gotoLogin(page: Page) {
  await page.goto('https://dev.anaonline.id/login');
}

export async function fillNpa(page: Page, npa: string) {
  await page.fill("#npa", npa);
}

export async function fillPassword(page: Page, password: string) {
  await page.fill("#password", password);
}

export async function submitLogin(page: Page) {
  await page.click('//button[@type="submit"]');
}

export async function login(page: Page, { npa, password }: { npa: string, password: string }) {
  await gotoLogin(page)
  await fillNpa(page, npa)
  await fillPassword(page, password)
  await submitLogin(page)
  await page.waitForTimeout(2000);
  await page.waitForURL('https://dev.anaonline.id/dashboard', { waitUntil: 'networkidle' });
}
